﻿using UnityEngine;
using System.Collections;

public class PlayerCursor: MonoBehaviour {
	public float speed = 90f;
	private Vector3 target;


	void Start () {
		target = new Vector2 (0f,-16f);
	}
	
	void FixedUpdate () {

		// Keeps the mouse within bounds
		if (Input.GetMouseButtonDown(0) && withinBoundsPlayable()) {
			target.x = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
			target.y = -16f;
			target.z = transform.position.z;
		}
		// Moves the mouse towards the position
		transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
	}  

	bool withinBoundsPlayable() {
		Vector3 mPos;
		mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		if ( mPos.x < 46f && mPos.x > -48f) {
			return true;
			}
		return false;
		}
}