﻿using UnityEngine;
using System.Collections;

public class BossScript : MonoBehaviour {
	public bool isAbleToMove = true;
	public GameObject explosionPrefab;
	public Transform[] waypoints;
	private float speed = 0.1f;
	public int point = 0;
	public bool isEnemy = true;	
	public bool shield = false;
	private float shootingCooldownMin = 1.0f; // minimum cooldown in seconds
	private float shootingCooldownMax = 1.5f; // maximum cooldown in seconds, not inclusive.
	public Rigidbody2D laserRigidbody2D; //Rigidbody2D goes here. Configurable Projectile shooter.
	public double aliveFrom = -2.0f; // total time alive. Set a negative number for time idle before shooting at 0.
	public int numberOfShots360 = 25;
	private int bossLife = 15;
	public bool bosslevel2 = false;
	public Sprite img1 , img2;
	void Start() 
	{
		Debug.Log ("Boss Sheild on");
		shield = true;
		StartCoroutine(spawnSheild());
	}

	void Update () {
		GameObject.Find ("GameController").GetComponent<B_GameGui>().guiBossLife.text = "Boss:" + bossLife.ToString();
		// Waypoint not reached yet? and is able to move, then move closer
		if(bosslevel2==true){
			setSprite();
			if(Time.time >= aliveFrom){ // Is cooldown finished?
				aliveFrom = Time.time + Random.Range(shootingCooldownMin,shootingCooldownMax); //Reset cooldown with randomizer
				// create laser
				bossShoot(numberOfShots360); // shoots lasers in a all directions
				Instantiate (explosionPrefab, transform.position, transform.rotation);
			}}

		if (transform.position != waypoints[point].position && isAbleToMove)
		{
			Vector2 path = Vector2.MoveTowards(transform.position,
			                                   waypoints[point].position,
			                                   speed);
			GetComponent<Rigidbody2D>().MovePosition(path);
		}
		else if (transform.position == waypoints[point].position && isAbleToMove)
		{
			// go to a random point
			point = Random.Range(0,waypoints.Length);
		}

	}

	public void setSprite(){
		gameObject.GetComponent<SpriteRenderer>().sprite = img2;
	}

	public IEnumerator spawnSheild(){
		shield = true;
		yield return new WaitForSeconds (0.6f);
		Debug.Log ("Boss Sheild off");
		shield = false;
	}

	void bossShoot(int numShots){
		numShots = (numShots % 46);// limit number of shots (0 ~ 45)
		float shotAngle = 360f / numShots;
		for (int i = 0; i < numShots; i++) {
			// Creates and shoots a configurable projectile
			Rigidbody2D laserClone = (Rigidbody2D)Instantiate (
			laserRigidbody2D, transform.position, transform.rotation);
			// spread over 360 degrees
			laserClone.GetComponent<laserScript> ().angle = shotAngle * (float)i; 
		}
	}

	void OnTriggerEnter2D(Collider2D otherCollider)
	{
		// Is this a shot?
		laserScript shot = otherCollider.gameObject.GetComponent<laserScript>();
		if (shot != null)
		{
			// Avoid friendly fire
			if (shot.isEnemyShot != isEnemy)
			{	if(shield==false){
					bossLife--;
					StartCoroutine(spawnSheild());
					if (bossLife<=0){
						Instantiate (explosionPrefab, transform.position, transform.rotation);
						Application.LoadLevel("Win");
						Destroy(gameObject);
						// Destroy the shot
						Destroy(shot.gameObject); // Remember to always target the game object, otherwise you will just remove the script
					}
				}
			}
		}
	}
	

}
