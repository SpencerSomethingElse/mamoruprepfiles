﻿using UnityEngine;
using System.Collections;

public class playershield : MonoBehaviour {
	public Sprite img1 , img2;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (GameObject.Find ("Player(Clone)").GetComponent<PlayerKeyboard> ().shield == true) {
			gameObject.GetComponent<SpriteRenderer>().sprite = img2;
		}else if (GameObject.Find ("Player(Clone)").GetComponent<PlayerKeyboard> ().shield == false){
			gameObject.GetComponent<SpriteRenderer>().sprite = img1;
		}
	}

}
