using UnityEngine;
using System.Collections;

/**
 *Straightforward basic unit

  Single enemy spawns from point. Travels down screen from point.

  Can be mass spawned.

  Two upgrades - decrease cooldown time, increase speed. At highest 
  level, should be equivalent to a bulkier laser.
 */
public class TrapMineScript : MonoBehaviour {
    // Moving points
	public bool ableToMove = true;
    public Transform[] waypoints;
    public int point;
    public float speed = 1f;
    public bool isEnemy = true;
	public Rigidbody2D laserRigidbody2D; //Rigidbody2D goes here. Configurable Projectile shooter.
	public double aliveFrom = -2.0f; // total time alive. Set a negative number for time idle before shooting at 0.
	public int numberOfShots360 = 8; // number of shots spread over 360 degrees. (0 ~ 45)
	public GameObject explosionPrefab;
	public GameObject BeamPowerUpPrefab;
	public GameObject ScatterPowerUpPrefab;

	void Start () {
		point = Random.Range(0,waypoints.Length);
	}

	void Update () {
        // Waypoint not reached yet? and is able to move,  then move closer
        if ( transform.position != waypoints[point].position && ableToMove)
        {
            Vector2 path = Vector2.MoveTowards(transform.position,
			                                waypoints[point].position,
                                            speed);
            GetComponent<Rigidbody2D>().MovePosition(path);
        } else {
			ableToMove = false;
		}



	}

    void OnTriggerEnter2D(Collider2D otherCollider)
    {
        // Is this a shot?
        laserScript shot = otherCollider.gameObject.GetComponent<laserScript>();
        if (shot != null)
        {
            // Avoid friendly fire from other enemies
            if (shot.isEnemyShot != isEnemy)
            {
				lasers360(numberOfShots360); // shoots lasers in a all directions
				Instantiate (explosionPrefab, transform.position, transform.rotation);
				if(Random.value <0.25){
					Instantiate (BeamPowerUpPrefab, transform.position, transform.rotation);
				}else if(Random.value >0.75){
					Instantiate (ScatterPowerUpPrefab, transform.position, transform.rotation);
				}
				Destroy(gameObject, .1f); // destroy this game object
                Destroy(shot.gameObject); // Remember to always target the game object, otherwise you will just remove the script
                
            }
        }
    }

	void lasers360 (int numShots) {
		numShots = (numShots % 46);// limit number of shots (0 ~ 45)
		float shotAngle = 360f/ numShots;
		for (int i = 0; i < numShots; i++ ){
			// Creates and shoots a configurable projectile
			Rigidbody2D laserClone = (Rigidbody2D) Instantiate(
				laserRigidbody2D, transform.position , transform.rotation);
			// spread over 360 degrees
			laserClone.GetComponent<laserScript>().angle = shotAngle *(float)i; 
		}
	}
}
