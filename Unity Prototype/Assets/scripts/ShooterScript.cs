﻿using UnityEngine;
using System.Collections;

/**
 *Straightforward basic unit

  Single enemy spawns from point. Travels down screen from point.

  Can be mass spawned.

  Two upgrades - decrease cooldown time, increase speed. At highest 
  level, should be equivalent to a bulkier laser.
 */
public class ShooterScript : MonoBehaviour {
    // Moving points
    public bool isAbleToMove = true;
    public Transform[] waypoints;
	public GameObject explosionPrefab;
    public float speed = 0.5f;
    public int point = 0;
    public bool isEnemy = true;
	public float shootingCooldownMin = 0.3f; // minimum cooldown in seconds
	public float shootingCooldownMax = 2.0f; // maximum cooldown in seconds, not inclusive.
	public Rigidbody2D laserRigidbody2D; //Rigidbody2D goes here. Configurable Projectile shooter.
	public double aliveFrom = -2.0f; // total time alive. Set a negative number for time idle before shooting at 0.
	public GameObject BeamPowerUpPrefab;
	public GameObject ScatterPowerUpPrefab;

    void Start (){
        point = Random.Range(0, waypoints.Length);
    }

	void Update () {
        // Waypoint not reached yet? and is able to move, then move closer
        if (transform.position != waypoints[point].position && isAbleToMove)
        {
            Vector2 path = Vector2.MoveTowards(transform.position,
                                            waypoints[point].position,
                                            speed);
            GetComponent<Rigidbody2D>().MovePosition(path);
        }
        else if (transform.position == waypoints[point].position && isAbleToMove)
        {
            // go to a random point
            point = (this.point + 1) % (waypoints.Length);
        }

		if(Time.time >= aliveFrom){ // Is cooldown finished?
			aliveFrom = Time.time + Random.Range(shootingCooldownMin,shootingCooldownMax); //Reset cooldown with randomizer
			// create laser
			Rigidbody2D laserClone = (Rigidbody2D) Instantiate(
				laserRigidbody2D, transform.position + (Vector3.down), transform.rotation);
			// point in opposite direction
			laserClone.GetComponent<laserScript>().angle = 180f; 
			laserClone.GetComponent<laserScript>().isEnemyShot = true;
		}
	}

    void OnTriggerEnter2D(Collider2D otherCollider)
    {
        // Is this a shot?
        laserScript shot = otherCollider.gameObject.GetComponent<laserScript>();
        if (shot != null)
        {
            // Avoid friendly fire
            if (shot.isEnemyShot != isEnemy)
            {
				Instantiate (explosionPrefab, transform.position, transform.rotation);
				if(Random.value <0.15){
					Instantiate (BeamPowerUpPrefab, transform.position, transform.rotation);
				}else if(Random.value >0.85){
					Instantiate (ScatterPowerUpPrefab, transform.position,transform.rotation);
				}
                Destroy(gameObject);
                // Destroy the shot
                Destroy(shot.gameObject); // Remember to always target the game object, otherwise you will just remove the script
            }
        }
    }


}
