﻿using UnityEngine;
using System.Collections;

/**
 *Straightforward basic unit

  Single enemy spawns from point. Travels down screen from point.

  Can be mass spawned.

  Two upgrades - decrease cooldown time, increase speed. At highest 
  level, should be equivalent to a bulkier laser.
 */
public class BasicEnemyScript : MonoBehaviour {
    // Moving points

    public bool isAbleToMove = true;
    public Transform[] waypoints;

    public float speed = 0.5f;
    public int point = 0;
    public bool isEnemy = true;

	void Update () {
        // Waypoint not reached yet? and is able to move,  then move closer
        if (transform.position != waypoints[point].position && isAbleToMove)
        {
            Vector2 path = Vector2.MoveTowards(transform.position,
                                            waypoints[point].position,
                                            speed);
            GetComponent<Rigidbody2D>().MovePosition(path);
        }
        else if (transform.position == waypoints[point].position && isAbleToMove)
        {
            // go to a random point
            point = Random.Range(0,waypoints.Length);
        }
	}

    void OnTriggerEnter2D(Collider2D otherCollider)
    {
        // Is this a shot?
        laserScript shot = otherCollider.gameObject.GetComponent<laserScript>();
        if (shot != null)
        {
            // Avoid friendly fire
            if (shot.isEnemyShot != isEnemy)
            {
                Destroy(gameObject);
                // Destroy the shot
                Destroy(shot.gameObject); // Remember to always target the game object, otherwise you will just remove the script
            }
        }
    }


}
