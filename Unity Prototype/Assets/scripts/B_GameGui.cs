using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class B_GameGui : MonoBehaviour
{
    public int life = 3;
    public Text guiLives;
    public Text guiWave;
    public Text guiMidWave;
    public Text ShotMode;
    public Text guiBossLife;
    public Image life1;
    public Image life2;
    public Image life3;
    // PREFABS
    public GameObject playerPrefab;
    public Rigidbody2D shooter0Prefab;
    public Rigidbody2D trapperPrefab;
    public Rigidbody2D trackerPrefab;
    public Rigidbody2D floaterPrefab;
    public Rigidbody2D bossPrefab;
    GameObject attachedPlayer = null;
    Rigidbody2D attachedShooter = null;
    Rigidbody2D attachedTrapper = null;
    Rigidbody2D attachedTracker = null;
    Rigidbody2D attachedFloater = null;
    int enemyWave = 1;
    public float enemySpawnTime = 3f;
    public Transform[] spawnPoints;
    // Use this for initialization

    void Start()
    {
        Destroy(GameObject.Find("Player(Clone)"));
        ShotMode.text = "Shot: single ";
        reSpawn();
        StartCoroutine(wave()); // Start the waves of enemies!
    }

    public void reSpawn()
    { //Create player ship
        if (playerPrefab == null)
        {
            return;
        }
        attachedPlayer = (GameObject)Instantiate(playerPrefab);
    }

    public void LoseLife()
    {//reduce 1 live if players has been hit. If there is no more lives. Game will be over.
        life--;
        if (life == 3)
        {
            life3.enabled = true;
            life2.enabled = true;
            life1.enabled = true;
        }
        else if (life == 2)
        {
            life3.enabled = false;
            life2.enabled = true;
            life1.enabled = true;
        }
        else if (life == 1)
        {
            life2.enabled = false;
            life1.enabled = true;
        }
        else if (life == 0)
        {
            life1.enabled = false;
        }

        if (life > 0)
        {
            reSpawn();
        }
        else
        {
            StopAllCoroutines();
            Application.LoadLevel("GameOver");
        }
    }

    public IEnumerator wave()
    {


        /** WAVE 1 **/
        
        guiMidWave.text = "Wave 1 incoming!";
		yield return new WaitForSeconds (2f);
        guiMidWave.text = "";
        yield return new WaitForSeconds(1f);
        // SPAWN
        spawnShooter0(1); // Spawns shooter0 at spawnPoint[1]
        yield return new WaitForSeconds(5f);
        spawnShooter0(0); // Spawns shooter0 at spawnPoint[0]
        yield return new WaitForSeconds(1f);
        spawnShooter0(3); // Spawns shooter0 at spawnPoint[3]
        yield return new WaitForSeconds(2f);
        spawnShooter0(2); // Spawns shooter0 at spawnPoint[2]
        yield return new WaitForSeconds(1f);
        spawnShooter0(1); // Spawns shooter0 at spawnPoint[1]
        yield return new WaitForSeconds(6f);
        spawnTrapper(3, 8);
        yield return new WaitForSeconds(4f);
        spawnTracker(2, .6f, 3);
        yield return new WaitForSeconds(4f);
        // End of wave and wait for approximate time for spawned enemies to die
        yield return new WaitForSeconds(1f);
        

        enemyWave = 2;
        spawnShooter0(0);
        yield return new WaitForSeconds(3f);
        Instantiate(shooter0Prefab, spawnPoints[2].position, spawnPoints[2].rotation);
        yield return new WaitForSeconds(3f);
        spawnShooter0(1);
        yield return new WaitForSeconds(3f);
        spawnShooter0(2);
        spawnShooter0(0);
        yield return new WaitForSeconds(4f);
        spawnShooter0(1);
        spawnShooter0(4);
        yield return new WaitForSeconds(5f);
        guiMidWave.text = "Wave " + enemyWave + " incoming! In 5s";
        yield return new WaitForSeconds(2f);
        guiMidWave.text = "";
        yield return new WaitForSeconds(3f);


        enemyWave = 3;
        spawnTracker(1, .6f, 5);
        yield return new WaitForSeconds(3f);
        spawnTracker(0, .3f, 15);
        spawnTracker(3, .3f, 15);
        yield return new WaitForSeconds(7f);
        spawnShooter0(0);
        spawnShooter0(3);
        guiMidWave.text = "Wave " + enemyWave + " incoming! In 5s";
        yield return new WaitForSeconds(2f);
        guiMidWave.text = "";
        yield return new WaitForSeconds(3f);


        enemyWave = 4;
        spawnFloater(0, 135, 1);
        spawnFloater(1, 135, 1);
        spawnFloater(2, 225, 1.5f);
        spawnFloater(4, 225, 1.5f);
        yield return new WaitForSeconds(4f);
        spawnFloater(0, 190, 1.5f);
        yield return new WaitForSeconds(1f);
        spawnFloater(2, 220, 1.5f);
        spawnFloater(3, 130, 1.3f);
        yield return new WaitForSeconds(2f);
        guiMidWave.text = "Wave " + enemyWave + " incoming! In 5s";
        yield return new WaitForSeconds(2f);
        guiMidWave.text = "";
        yield return new WaitForSeconds(1f);


        enemyWave = 5;
        spawnTrapper(0, 18);
        spawnTrapper(3, 2);
        yield return new WaitForSeconds(5f);
        spawnTracker(3, .7f ,20);
        yield return new WaitForSeconds(4f);
        spawnShooter0(0);
        spawnShooter0(1);
        spawnShooter0(2);
        spawnShooter0(3);
        yield return new WaitForSeconds(4f);
        spawnShooter0(0);
        spawnShooter0(0);
        spawnShooter0(1);
        spawnShooter0(2);
        spawnShooter0(3);
        spawnShooter0(3);
        yield return new WaitForSeconds(2f);
        guiMidWave.text = "Wave " + enemyWave + " incoming! In 5s";
        yield return new WaitForSeconds(2f);
        guiMidWave.text = "";
        yield return new WaitForSeconds(3f);


        enemyWave = 6;
        spawnShooter0(0);
        spawnShooter0(1);
        spawnShooter0(2);
        spawnShooter0(4);
        yield return new WaitForSeconds(3f);
        spawnFloater(0, 175, 1f);
        spawnFloater(1, 175, 1f);
        yield return new WaitForSeconds(1f);
        spawnFloater(0, 175, 1f);
        spawnFloater(1, 175, 1f);
        yield return new WaitForSeconds(2f);
        spawnFloater(2, 215, 1f);
        spawnFloater(3, 215, 1f);
        yield return new WaitForSeconds(1f);
        spawnFloater(2, 215, 1f);
        spawnFloater(3, 215, 1f);
        yield return new WaitForSeconds(2f);
        spawnTracker(0, .5f, 5);
        spawnTracker(1, .6f, 5);
        spawnTracker(2, .6f, 5);
        spawnTracker(3, .5f, 5);
        guiMidWave.text = "Wave " + enemyWave + " incoming! In 5s";
        yield return new WaitForSeconds(2f);
        guiMidWave.text = "";
        yield return new WaitForSeconds(6f);

        /**
        enemyWave = 7;
        attachedShooter = (Rigidbody2D)Instantiate(shooter0Prefab);
        attachedShooter = (Rigidbody2D)Instantiate(shooter0Prefab);
        attachedShooter = (Rigidbody2D)Instantiate(shooter0Prefab);
        attachedShooter = (Rigidbody2D)Instantiate(shooter0Prefab);
        attachedShooter = (Rigidbody2D)Instantiate(shooter0Prefab);
        attachedShooter = (Rigidbody2D)Instantiate(shooter0Prefab);
        attachedTrapper = (Rigidbody2D)Instantiate(trapperPrefab);
        guiMidWave.text = "Wave " + enemyWave + " incoming! In 5s";
        yield return new WaitForSeconds(2f);
        guiMidWave.text = "";
        yield return new WaitForSeconds(3f);

        enemyWave = 8;
        attachedTrapper = (Rigidbody2D)Instantiate(trapperPrefab);
        attachedTrapper = (Rigidbody2D)Instantiate(trapperPrefab);
        attachedTracker = (Rigidbody2D)Instantiate(trackerPrefab);
        attachedTracker = (Rigidbody2D)Instantiate(trackerPrefab);
        attachedTracker = (Rigidbody2D)Instantiate(trackerPrefab);
        guiMidWave.text = "Wave " + enemyWave + " incoming! In 5s";
        yield return new WaitForSeconds(2f);
        guiMidWave.text = "";
        yield return new WaitForSeconds(3f);


        enemyWave = 9;
        attachedTrapper = (Rigidbody2D)Instantiate(trapperPrefab);
        attachedTrapper = (Rigidbody2D)Instantiate(trapperPrefab);
        attachedTracker = (Rigidbody2D)Instantiate(trackerPrefab);
        attachedTracker = (Rigidbody2D)Instantiate(trackerPrefab);
        attachedTracker = (Rigidbody2D)Instantiate(trackerPrefab);
        guiMidWave.text = "Wave " + enemyWave + " incoming! In 5s";
        yield return new WaitForSeconds(2f);
        guiMidWave.text = "";
        yield return new WaitForSeconds(3f);

        enemyWave = 10;
        attachedTrapper = (Rigidbody2D)Instantiate(trapperPrefab);
        attachedTrapper = (Rigidbody2D)Instantiate(trapperPrefab);
        attachedTracker = (Rigidbody2D)Instantiate(trackerPrefab);
        attachedTracker = (Rigidbody2D)Instantiate(trackerPrefab);
        attachedTracker = (Rigidbody2D)Instantiate(trackerPrefab);
        guiMidWave.text = "Wave " + enemyWave + " incoming! In 5s";
        yield return new WaitForSeconds(2f);
        guiMidWave.text = "";
        yield return new WaitForSeconds(3f);
        **/
        enemyWave = 11;
        guiMidWave.text = "Warning!!! Warning!!!";
        yield return new WaitForSeconds(3f);
        guiMidWave.text = "The BOSS IS HERE!";
        yield return new WaitForSeconds(3f);
        Instantiate(bossPrefab, spawnPoints[4].position, spawnPoints[4].rotation);
        yield return new WaitForSeconds(3f);
        spawnTracker(0, .1f, 1);
        guiMidWave.text = "";
        yield return new WaitForSeconds(5f);
        
        spawnTracker(0, .1f, 1);
        yield return new WaitForSeconds(5f);
        spawnTracker(0, .1f, 1);
        yield return new WaitForSeconds(5f);
        spawnTracker(0, .1f, 1);
        yield return new WaitForSeconds(5f);
        spawnTracker(0, .1f, 1);
        yield return new WaitForSeconds(5f);
    }

    /**
	 * Spawns a shooter with configurable stats
	 */
    void spawnShooter0(int pointNum, float setSpeed, float min, float max)
    {
        attachedShooter = (Rigidbody2D)Instantiate(
            shooter0Prefab, spawnPoints[pointNum].position, spawnPoints[pointNum].rotation);

        // set stats
        attachedShooter.GetComponent<ShooterScript>().speed = setSpeed;
        attachedShooter.GetComponent<ShooterScript>().shootingCooldownMin = min;
        attachedShooter.GetComponent<ShooterScript>().shootingCooldownMax = max;

    }

    /**
	 * Spawns a shooter at spawn position
	 */
    void spawnShooter0(int pointNum)
    {
        attachedShooter = (Rigidbody2D)Instantiate(
            shooter0Prefab, spawnPoints[pointNum].position, spawnPoints[pointNum].rotation);
    }

    /**
	 * Spawns a tracker
	 */
    void spawnTracker(int pointNum, float setSpeed, int HP)
    {
        attachedTracker = (Rigidbody2D)Instantiate(
            trackerPrefab, spawnPoints[pointNum].position, spawnPoints[pointNum].rotation);

        // set stats
        attachedTracker.GetComponent<TrackerScript>().speed = setSpeed;
        attachedTracker.GetComponent<TrackerScript>().trackerHp = HP;
    }

    /**
	 * Spawns a TrapMine
	 */
    void spawnTrapper(int pointNum, int numOfShots360)
    {
        attachedTrapper = (Rigidbody2D)Instantiate(
            trapperPrefab, spawnPoints[pointNum].position, spawnPoints[pointNum].rotation);

        // set stats
        attachedTrapper.GetComponent<TrapMineScript>().numberOfShots360 = numOfShots360;
    }

    /**
	 * Spawns a Floater
	 */
    void spawnFloater(int pointNum, int angle, float speed)
    {
        attachedFloater = (Rigidbody2D)Instantiate(
            floaterPrefab, spawnPoints[pointNum].position, spawnPoints[pointNum].rotation);

        //set stats
        attachedFloater.GetComponent<FloaterScript>().angle = angle; // default is 20f
        attachedFloater.GetComponent<FloaterScript>().lasSpeed = 20f * speed; // default is 20f
        attachedFloater.GetComponent<FloaterScript>().secondsAlive = (int)(speed / 0.1f); // default is 20f

    }


    void Update()
    {
        guiWave.text = "Wave: " + enemyWave.ToString();
    }
}
