﻿using UnityEngine;
using System.Collections;

public class Boss_feet : MonoBehaviour {
	public bool isAbleToMove = true;
	public GameObject explosionPrefab;
	private float speed = 1f;
	public int point = 0;
	public bool isEnemy = true;	
	public bool shield = false;
	private float shootingCooldownMin = 1.5f; // minimum cooldown in seconds
	private float shootingCooldownMax = 2.5f; // maximum cooldown in seconds, not inclusive.
	public Rigidbody2D laserRigidbody2D; //Rigidbody2D goes here. Configurable Projectile shooter.
	public double aliveFrom = -2.0f; // total time alive. Set a negative number for time idle before shooting at 0.
	private int bossLife = 10;
	// Use this for initialization
	void Start () {
		Debug.Log ("Boss Sheild on");
		shield = true;
		StartCoroutine(spawnSheild());
	}
	
	void Update () {
		//GameObject.Find ("GameController").GetComponent<B_GameGui>().guiBossLife.text = "feet1:" + bossLife.ToString();
		if(Time.time >= aliveFrom){ // Is cooldown finished?
			aliveFrom = Time.time + Random.Range(shootingCooldownMin,shootingCooldownMax); //Reset cooldown with randomizer
			// create laser
			Rigidbody2D laserClone = (Rigidbody2D) Instantiate(
				laserRigidbody2D, transform.position + (Vector3.down), transform.rotation);
			// point in opposite direction
			laserClone.GetComponent<laserScript>().angle = 180f; 
			laserClone.GetComponent<laserScript>().isEnemyShot = true;
		}

		
	}
	
	public IEnumerator spawnSheild(){
		shield = true;
		yield return new WaitForSeconds (0.3f);
		Debug.Log ("Boss Sheild off");
		shield = false;
	}
	

	void OnTriggerEnter2D(Collider2D otherCollider)
	{
		// Is this a shot?
		laserScript shot = otherCollider.gameObject.GetComponent<laserScript>();
		if (shot != null)
		{
			// Avoid friendly fire
			if (shot.isEnemyShot != isEnemy)
			{	if(shield==false){
					bossLife--;
					StartCoroutine(spawnSheild());
					if (bossLife<=0){
						Instantiate (explosionPrefab, transform.position, transform.rotation);
						GameObject.Find ("EnemyBossShield").GetComponent<bossShield>().reduceBS();
						Destroy(gameObject);
						// Destroy the shot
						Destroy(shot.gameObject); // Remember to always target the game object, otherwise you will just remove the script
					}
				}
			}
		}
	}
}
