﻿using UnityEngine;
using System.Collections;

public class laserScript : MonoBehaviour {
    public float angle = 0f % 360f; // rotates in a clockwise angle in degrees.
    public float lasSpeed = 40f; // speed of the laser
	public int damage = 1; // Amount of damage it does
    private Vector2 pos;
    private RaycastHit2D hit;
	public bool isEnemyShot = false; // If it is an enemy's shot?
    public int secondsAlive = 1; // Seconds alive for before it dies.
    public GameObject explosionPrefab;
    GameObject attachedExplosion = null;

	// Use this for initialization
	void Start () {
        pos = transform.position; // initial position
		Destroy(gameObject, secondsAlive); //destroy it after 5 sec if it didn't hit enemy.
		setAngle(angle); // Rotate entire object in degrees
        
	}

	// Update is called once per frame
	void Update () {
        // Constantly moves upwards
        transform.Translate(0, lasSpeed * Time.deltaTime, 0);

	}

    void OnTriggerEnter2D(Collider2D otherCollider)
    {
        // Destroys player
        PlayerKeyboard player = otherCollider.gameObject.GetComponent<PlayerKeyboard>();
		GameObject playerLife= GameObject.Find("Player(Clone)");
		PlayerKeyboard reduceLife= playerLife.gameObject.GetComponent<PlayerKeyboard>();
		if (player != null && isEnemyShot)// PLAYER.LOSE LIFE
        {
			if(reduceLife.shield == false){
				Instantiate(explosionPrefab, transform.position, transform.rotation);
				reduceLife.deductLife ();
				Destroy(gameObject);
			}           
        }

        // When shot by player, it should die in contact with anything but the player.
        if (!isEnemyShot && otherCollider.tag == "enemy")
        {
            Instantiate(explosionPrefab, transform.position, transform.rotation);
            Destroy(gameObject);
        }

    }
	void setAngle(float yaw) // Yaw, rotates around the Z axis
	{
		transform.rotation = Quaternion.AngleAxis(yaw,Vector3.back); 
	}
}
