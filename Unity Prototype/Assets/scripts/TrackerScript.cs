using UnityEngine;
using System.Collections;

public class TrackerScript : MonoBehaviour {
	public float speed = 0.5f;
	public int point = 0;
	public int trackerHp=1;
	public bool isEnemy = true;
	public GameObject explosionPrefab;
	public bool isEnemyShot = false; // If it is an enemy's shot?
	public GameObject BeamPowerUpPrefab;
	public GameObject ScatterPowerUpPrefab;

	public void Damage(int damageCount){
		trackerHp -= damageCount;
		if (trackerHp <= 0) {
		//Dead!
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D otherCollider)
	{
		// Is this a shot?
		laserScript shot = otherCollider.gameObject.GetComponent<laserScript>();
		if (shot != null) {
			// Avoid friendly fire
			if (shot.isEnemyShot != isEnemy)
			{
				Instantiate (explosionPrefab, transform.position, transform.rotation);
				if(Random.value <0.10){
					Instantiate (BeamPowerUpPrefab, transform.position, transform.rotation);
				}else if(Random.value >0.90){
					Instantiate (ScatterPowerUpPrefab, transform.position, transform.rotation);
				}
				Damage(shot.damage);
				// Destroy the shot
				Destroy(shot.gameObject); // Remember to always target the game object, otherwise you will just remove the script
			}
		}

		PlayerKeyboard player = otherCollider.gameObject.GetComponent<PlayerKeyboard>();
		if (player != null && isEnemyShot)
		{
			// PLAYER.LOSE LIFE
			GameObject playerLife= GameObject.Find("Player(Clone)");
			PlayerKeyboard reduceLife= playerLife.gameObject.GetComponent<PlayerKeyboard>();
			reduceLife.deductLife ();
			Destroy(this.gameObject);
		}
	}


	void FixedUpdate()
	{
//		barPos = GameObject.FindWithTag("Player").transform.position;
	}
	// Update is called once per frame
	void Update () {
		Vector2 p = Vector2.MoveTowards(transform.position,GameObject.FindWithTag("Player").transform.position,speed);
		GetComponent<Rigidbody2D>().MovePosition(p);
	}

}
