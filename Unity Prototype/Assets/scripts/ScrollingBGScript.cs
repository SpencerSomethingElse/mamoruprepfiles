﻿using UnityEngine;
using System.Collections;

public class ScrollingBGScript : MonoBehaviour {
	public float speed = -0.2f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Renderer>().material.SetTextureOffset(
			"_MainTex",
			new Vector2(0.0f, speed * Time.time));
	}
}
